<?php

declare(strict_types=1);

namespace Migrations;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
final class Version20200518105343 extends AbstractMigration
{
    public function getDescription(): string
    {
        return '';
    }

    public function up(Schema $schema): void
    {
        // this up() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() !== 'mysql', 'Migration can only be executed safely on \'mysql\'.');

        $this->addSql('CREATE TABLE portfolio_type (id INT AUTO_INCREMENT NOT NULL, webalize VARCHAR(255) NOT NULL, name VARCHAR(255) NOT NULL, content LONGTEXT NOT NULL, sequence INT NOT NULL, PRIMARY KEY(id)) DEFAULT CHARACTER SET UTF8 COLLATE `UTF8_unicode_ci` ENGINE = InnoDB');
        $this->addSql('ALTER TABLE portfolio ADD portfolio_type_id INT DEFAULT NULL');
        $this->addSql('ALTER TABLE portfolio ADD CONSTRAINT FK_A9ED106293F9A35D FOREIGN KEY (portfolio_type_id) REFERENCES portfolio_type (id) ON DELETE CASCADE');
        $this->addSql('CREATE INDEX IDX_A9ED106293F9A35D ON portfolio (portfolio_type_id)');

        $translations = [
            ['original' => 'grid.portfolio.overview.action.portfolio-type', 'hash' => '3e46d51ce602e0f3acbdb963e513815c', 'module' => 'admin', 'language_id' => 1, 'singular' => 'Typy portfolií', 'plural1' => '', 'plural2' => ''],
            ['original' => 'portfolio.overview-type.title', 'hash' => '4d649a764df8427a03fd40e7e01f93f4', 'module' => 'admin', 'language_id' => 1, 'singular' => 'Typy portfolií|Přehled', 'plural1' => '', 'plural2' => ''],
            ['original' => 'grid.portfolio.overview-type.name', 'hash' => '29a08e7674feb36c645dade840012f07', 'module' => 'admin', 'language_id' => 1, 'singular' => 'Název', 'plural1' => '', 'plural2' => ''],
            ['original' => 'grid.portfolio.overview-type.action.edit', 'hash' => 'fdd79bb7007c067118cd7a54b3adbd2f', 'module' => 'admin', 'language_id' => 1, 'singular' => '', 'plural1' => '', 'plural2' => ''],
            ['original' => 'grid.portfolio.overview.action.portfolio', 'hash' => '405869e1287ba5f5d08927a4fe6bc065', 'module' => 'admin', 'language_id' => 1, 'singular' => 'Zpět na přehled', 'plural1' => '', 'plural2' => ''],
            ['original' => 'grid.portfolio.overview-type.action.new', 'hash' => '3713a9d0d251f734795381219ad0ae6e', 'module' => 'admin', 'language_id' => 1, 'singular' => 'Založit typ portfolia', 'plural1' => '', 'plural2' => ''],
            ['original' => 'grid.portfolio.overview-type.action.flash.sort.success', 'hash' => 'ed546347e1ae60c1a91f7df26f7fef8d', 'module' => 'admin', 'language_id' => 1, 'singular' => 'Pořadí typů porfolií bylo upraveno.', 'plural1' => '', 'plural2' => ''],
            ['original' => 'portfolio-type.edit.title - %s', 'hash' => '103c1d077804aca0d9f19fff893cdf1f', 'module' => 'admin', 'language_id' => 1, 'singular' => 'Typ portofila %s|Editace', 'plural1' => '', 'plural2' => ''],
            ['original' => 'portfolio-type.edit.title', 'hash' => '6b6eda34460d20b1d04a7d5fb5797ef9', 'module' => 'admin', 'language_id' => 1, 'singular' => 'Založení typu portfolia', 'plural1' => '', 'plural2' => ''],
            ['original' => 'form.portfolio-type.edit.flash.success.update', 'hash' => '9f6b30aeb09cac64167852911b552c13', 'module' => 'admin', 'language_id' => 1, 'singular' => 'Typ portfolia byl úspěšně upraven.', 'plural1' => '', 'plural2' => ''],
            ['original' => 'form.portfolio-type.edit.flash.success.create', 'hash' => 'd5e20d6eacd32e340c33df3ef424b1e8', 'module' => 'admin', 'language_id' => 1, 'singular' => 'Typ portfolia byl úspěšně založen.', 'plural1' => '', 'plural2' => ''],
            ['original' => 'form.portfolio-type.edit.name', 'hash' => '746205b00af0a4e9a71a4717ff55fa3e', 'module' => 'admin', 'language_id' => 1, 'singular' => 'Název', 'plural1' => '', 'plural2' => ''],
            ['original' => 'form.portfolio-type.edit.name.req', 'hash' => 'f9bfacaff9c7fd2db7c6452af937294f', 'module' => 'admin', 'language_id' => 1, 'singular' => 'Zadejte prosím název', 'plural1' => '', 'plural2' => ''],
            ['original' => 'form.portfolio-type.edit.content', 'hash' => '7bb4241fbd2fe69c08e9a3a1e58001c1', 'module' => 'admin', 'language_id' => 1, 'singular' => 'Obsah', 'plural1' => '', 'plural2' => ''],
            ['original' => 'form.portfolio-type.edit.send', 'hash' => 'fa03049e1e1a2c3e81b291893c2c0b9a', 'module' => 'admin', 'language_id' => 1, 'singular' => 'Uložit', 'plural1' => '', 'plural2' => ''],
            ['original' => 'form.portfolio-type.edit.send-back', 'hash' => '4eb3343df73eeed695b96c99609ae0ba', 'module' => 'admin', 'language_id' => 1, 'singular' => 'Uložit a zpět', 'plural1' => '', 'plural2' => ''],
            ['original' => 'form.portfolio-type.edit.back', 'hash' => 'a44af5473ad8a2bc89a58e9353a5f719', 'module' => 'admin', 'language_id' => 1, 'singular' => 'Zpět', 'plural1' => '', 'plural2' => ''],
            ['original' => 'form.portfolio.edit.portfolio-type-id', 'hash' => '6631f13fa223a9109e007d130868b038', 'module' => 'admin', 'language_id' => 1, 'singular' => 'Typ portofila', 'plural1' => '', 'plural2' => ''],
            ['original' => 'form.portfolio.edit.portfolio-type-id.req', 'hash' => '2de0bb5f1fde7e9701e0024f191a242c', 'module' => 'admin', 'language_id' => 1, 'singular' => 'Vyberte prosím typ portofila', 'plural1' => '', 'plural2' => ''],
            ['original' => 'form.portfolio.edit.change-webalize', 'hash' => '35146c670bde4b0ce4e3e8ca1dceb253', 'module' => 'admin', 'language_id' => 1, 'singular' => 'Znovu vygenerovat url', 'plural1' => '', 'plural2' => ''],
            ['original' => 'form.portfolio-type.edit.change-webalize', 'hash' => '9575a94cbdc2808b5efeb4cd4cdbfda6', 'module' => 'admin', 'language_id' => 1, 'singular' => 'Znovu vygenerovat url', 'plural1' => '', 'plural2' => ''],
            ['original' => 'grid.portfolio.overview.portfolio-type', 'hash' => 'd8eb65c83f08a1184c6befd8d9bee11a', 'module' => 'admin', 'language_id' => 1, 'singular' => 'Typ', 'plural1' => '', 'plural2' => ''],
            ['original' => 'grid.portfolio.overview.title', 'hash' => 'c05c4dc1eaedc1787fa555733009e3b7', 'module' => 'admin', 'language_id' => 1, 'singular' => 'Podnadpis', 'plural1' => '', 'plural2' => ''],
        ];

        foreach ($translations as $translation) {
            $this->addSql('DELETE FROM translation WHERE hash = :hash', $translation);
            $this->addSql('SELECT create_translation(:original, :hash, :module, :language_id, :singular, :plural1, :plural2)', $translation);
        }
    }

    public function down(Schema $schema): void
    {
        // this down() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() !== 'mysql', 'Migration can only be executed safely on \'mysql\'.');

        $this->addSql('ALTER TABLE portfolio DROP FOREIGN KEY FK_A9ED106293F9A35D');
        $this->addSql('DROP TABLE portfolio_type');
        $this->addSql('DROP INDEX IDX_A9ED106293F9A35D ON portfolio');
        $this->addSql('ALTER TABLE portfolio DROP portfolio_type_id');
    }
}
