<?php

declare(strict_types=1);

namespace Migrations;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;
use Skadmin\Portfolio\BaseControl;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
final class Version20200430110332 extends AbstractMigration
{
    public function getDescription(): string
    {
        return '';
    }

    public function up(Schema $schema): void
    {
        $data = [
            'base_control' => BaseControl::class,
            'webalize'     => 'portfolio',
            'name'         => 'Portfolio',
            'is_active'    => 1,
        ];
        $this->addSql(
            'INSERT INTO core_package (base_control, webalize, name, is_active) VALUES (:base_control, :webalize, :name, :is_active)',
            $data
        );

        $resource = [
            'name'        => 'portfolio',
            'title'       => 'role-resource.portfolio.title',
            'description' => 'role-resource.portfolio.description',
        ];
        $this->addSql('INSERT INTO core_role_resource (name, title, description) VALUES (:name, :title, :description)', $resource);
    }

    public function down(Schema $schema): void
    {
    }
}
