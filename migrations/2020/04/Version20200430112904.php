<?php

declare(strict_types=1);

namespace Migrations;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
final class Version20200430112904 extends AbstractMigration
{
    public function getDescription(): string
    {
        return '';
    }

    public function up(Schema $schema): void
    {
        $translations = [
            ['original' => 'portfolio.overview', 'hash' => '5f003dae220d51db6ddba850c46031da', 'module' => 'admin', 'language_id' => 1, 'singular' => 'Portfolio', 'plural1' => '', 'plural2' => ''],
            ['original' => 'role-resource.portfolio.title', 'hash' => '98650636e843a99f6e3df78b4628e25b', 'module' => 'admin', 'language_id' => 1, 'singular' => 'Portfolio', 'plural1' => '', 'plural2' => ''],
            ['original' => 'role-resource.portfolio.description', 'hash' => 'f64e5e26345afe28d877bd21817387b5', 'module' => 'admin', 'language_id' => 1, 'singular' => 'Umožňuje spravovat portfolio', 'plural1' => '', 'plural2' => ''],
            ['original' => 'portfolio.overview.title', 'hash' => '3f6ebe0a71dbed8b6dad62080babb837', 'module' => 'admin', 'language_id' => 1, 'singular' => 'Portfolio|Přehled', 'plural1' => '', 'plural2' => ''],
            ['original' => 'grid.portfolio.overview.is-active', 'hash' => 'c7a5b6140a9c6d1c6bf658d55c359454', 'module' => 'admin', 'language_id' => 1, 'singular' => 'Je aktivní?', 'plural1' => '', 'plural2' => ''],
            ['original' => 'grid.portfolio.overview.action.new', 'hash' => 'd0ae756210b789600b98818f44a09d16', 'module' => 'admin', 'language_id' => 1, 'singular' => 'Založit portfolio', 'plural1' => '', 'plural2' => ''],
            ['original' => 'grid.portfolio.overview.name', 'hash' => 'fa893e5bee516568a940f22de79471c7', 'module' => 'admin', 'language_id' => 1, 'singular' => 'Název', 'plural1' => '', 'plural2' => ''],
            ['original' => 'grid.portfolio.overview.action.edit', 'hash' => '666db6154acb9c2ec445a53d74e5b086', 'module' => 'admin', 'language_id' => 1, 'singular' => '', 'plural1' => '', 'plural2' => ''],
            ['original' => 'portfolio.edit.title', 'hash' => 'a705bb29529722aa3243da3fa6bb6caf', 'module' => 'admin', 'language_id' => 1, 'singular' => 'Založení portoflia', 'plural1' => '', 'plural2' => ''],
            ['original' => 'form.portfolio.edit.name', 'hash' => 'c7ea899d6d53aeb9fe259b43358f78c6', 'module' => 'admin', 'language_id' => 1, 'singular' => 'Název', 'plural1' => '', 'plural2' => ''],
            ['original' => 'form.portfolio.edit.name.req', 'hash' => '4eb0e615d325b6d4e2578652c1a065af', 'module' => 'admin', 'language_id' => 1, 'singular' => 'Zadejte prosím název', 'plural1' => '', 'plural2' => ''],
            ['original' => 'form.portfolio.edit.title', 'hash' => '30e8729de6b2e73bf36d7e3e72477788', 'module' => 'admin', 'language_id' => 1, 'singular' => 'Podnapis', 'plural1' => '', 'plural2' => ''],
            ['original' => 'form.portfolio.edit.image-preview', 'hash' => '7957e2b24fbe825346b8db3c8a8c1460', 'module' => 'admin', 'language_id' => 1, 'singular' => 'Náhled portfolia', 'plural1' => '', 'plural2' => ''],
            ['original' => 'form.portfolio.edit.image-preview.rule-image', 'hash' => '85948ed8c5e948c075c42aaf05537465', 'module' => 'admin', 'language_id' => 1, 'singular' => 'Vybraný soubor není platný obrázek', 'plural1' => '', 'plural2' => ''],
            ['original' => 'form.portfolio.edit.content', 'hash' => 'd0c21bb5cf8776daf7e4938c8898ffa9', 'module' => 'admin', 'language_id' => 1, 'singular' => 'Obsah', 'plural1' => '', 'plural2' => ''],
            ['original' => 'form.portfolio.edit.is-active', 'hash' => '94687dfd70626af6e7b12f685df53b9b', 'module' => 'admin', 'language_id' => 1, 'singular' => 'Je aktivní', 'plural1' => '', 'plural2' => ''],
            ['original' => 'form.portfolio.edit.send', 'hash' => 'e9fd5e6577e00d444a51962514b4a68e', 'module' => 'admin', 'language_id' => 1, 'singular' => 'Uložit', 'plural1' => '', 'plural2' => ''],
            ['original' => 'form.portfolio.edit.send-back', 'hash' => 'ed4775407e9e20630767a4dd6cd8608b', 'module' => 'admin', 'language_id' => 1, 'singular' => 'Ulož a zpět', 'plural1' => '', 'plural2' => ''],
            ['original' => 'form.portfolio.edit.back', 'hash' => '03a251b10c234a19f1e68f7c42be7226', 'module' => 'admin', 'language_id' => 1, 'singular' => 'Zpět', 'plural1' => '', 'plural2' => ''],
            ['original' => 'portfolio.edit.title - %s', 'hash' => '437dc8078b9e7d69d72a332f5fa2f059', 'module' => 'admin', 'language_id' => 1, 'singular' => '%s|Editace portfolia', 'plural1' => '', 'plural2' => ''],
            ['original' => 'form.portfolio.edit.flash.success.create', 'hash' => '923ecee5e45f7d237182493d517dbbf5', 'module' => 'admin', 'language_id' => 1, 'singular' => 'Portfolio bylo úspěšně založeno.', 'plural1' => '', 'plural2' => ''],
            ['original' => 'form.portfolio.edit.flash.success.update', 'hash' => 'ca7d05695b76bf988594b717883178bd', 'module' => 'admin', 'language_id' => 1, 'singular' => 'Portfolio bylo úspěšně upraveno.', 'plural1' => '', 'plural2' => ''],
            ['original' => 'grid.portfolio.overview.action.flash.sort.success', 'hash' => '50e7f7518b4789d8c948944793cc07a6', 'module' => 'admin', 'language_id' => 1, 'singular' => 'Pořadí portfolií bylo upraveno.', 'plural1' => '', 'plural2' => ''],
        ];

        foreach ($translations as $translation) {
            $this->addSql('DELETE FROM translation WHERE hash = :hash', $translation);
            $this->addSql('SELECT create_translation(:original, :hash, :module, :language_id, :singular, :plural1, :plural2)', $translation);
        }
    }

    public function down(Schema $schema): void
    {
    }
}
