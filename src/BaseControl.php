<?php

declare(strict_types=1);

namespace Skadmin\Portfolio;

use App\Model\System\ABaseControl;
use Nette\Utils\ArrayHash;
use Nette\Utils\Html;

class BaseControl extends ABaseControl
{
    public const RESOURCE  = 'portfolio';
    public const DIR_IMAGE = 'portfolio';

    public function getMenu(): ArrayHash
    {
        return ArrayHash::from([
            'control' => $this,
            'icon'    => Html::el('i', ['class' => 'fas fa-fw fa-solar-panel']),
            'items'   => ['overview'],
        ]);
    }
}
