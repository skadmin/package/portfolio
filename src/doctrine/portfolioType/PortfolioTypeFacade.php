<?php

declare(strict_types=1);

namespace Skadmin\Portfolio\Doctrine\PortfolioType;

use Nettrine\ORM\EntityManagerDecorator;
use SkadminUtils\DoctrineTraits;
use SkadminUtils\DoctrineTraits\Facade;

final class PortfolioTypeFacade extends Facade
{
    use DoctrineTraits\Facade\Webalize;
    use DoctrineTraits\Facade\Sequence;

    public function __construct(EntityManagerDecorator $em)
    {
        parent::__construct($em);
        $this->table = PortfolioType::class;
    }

    public function create(string $name, string $content): PortfolioType
    {
        return $this->update(null, $name, $content);
    }

    public function update(?int $id, string $name, string $content, bool $changeWebalize = false): PortfolioType
    {
        $portfolioType = $this->get($id);
        $portfolioType->update($name, $content);

        if (! $portfolioType->isLoaded()) {
            $portfolioType->setWebalize($this->getValidWebalize($name));
            $portfolioType->setSequence($this->getValidSequence());
        } elseif ($changeWebalize) {
            $portfolioType->updateWebalize($this->getValidWebalize($name));
        }

        $this->em->persist($portfolioType);
        $this->em->flush();

        return $portfolioType;
    }

    public function get(?int $id = null): PortfolioType
    {
        if ($id === null) {
            return new PortfolioType();
        }

        $portfolioType = parent::get($id);

        if ($portfolioType === null) {
            return new PortfolioType();
        }

        return $portfolioType;
    }

    /**
     * @return PortfolioType[]
     */
    public function getAll(): array
    {
        $criteria = [];
        $orderBy  = ['sequence' => 'ASC'];

        return $this->em
            ->getRepository($this->table)
            ->findBy($criteria, $orderBy);
    }

    public function findByWebalize(string $webalize): ?PortfolioType
    {
        $criteria = ['webalize' => $webalize];

        return $this->em
            ->getRepository($this->table)
            ->findOneBy($criteria);
    }
}
