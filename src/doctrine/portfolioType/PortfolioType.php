<?php

declare(strict_types=1);

namespace Skadmin\Portfolio\Doctrine\PortfolioType;

use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\Common\Collections\Criteria;
use Doctrine\ORM\Mapping as ORM;
use Skadmin\Portfolio\Doctrine\Portfolio\Portfolio;
use SkadminUtils\DoctrineTraits\Entity;

#[ORM\Entity]
#[ORM\Table(name: 'portfolio_type')]
#[ORM\HasLifecycleCallbacks]
class PortfolioType
{
    use Entity\Id;
    use Entity\WebalizeName;
    use Entity\Content;
    use Entity\Sequence;

    /** @var ArrayCollection|Portfolio[] */
    #[ORM\OneToMany(targetEntity: Portfolio::class, mappedBy: 'portfolioType')]
    #[ORM\OrderBy(['sequence' => 'ASC', 'name' => 'ASC'])]
    private ArrayCollection|Collection|array $portfolios;

    public function __construct()
    {
        $this->portfolios = new ArrayCollection();
    }

    public function update(string $name, string $content): void
    {
        $this->name    = $name;
        $this->content = $content;
    }

    public function updateWebalize(string $webalize): void
    {
        $this->webalize = $webalize;
    }

    /**
     * @return ArrayCollection|Portfolio[]
     */
    public function getportfolios(bool $onlyActive = false): ArrayCollection|array
    {
        if (! $onlyActive) {
            return $this->portfolios;
        }

        $criteria = Criteria::create()->where(Criteria::expr()->eq('isActive', true));

        return $this->portfolios->matching($criteria);
    }

    /**
     * Fire trigger every time on update
     *
     * @Doctrine\ORM\Mapping\PreUpdate
     * @Doctrine\ORM\Mapping\PrePersist
     */
    public function onPreUpdateWebalizeName(): void
    {
    }
}
