<?php

declare(strict_types=1);

namespace Skadmin\Portfolio\Doctrine\Portfolio;

use Doctrine\ORM\Mapping as ORM;
use Skadmin\Portfolio\Doctrine\PortfolioType\PortfolioType;
use SkadminUtils\DoctrineTraits\Entity;

#[ORM\Entity]
#[ORM\HasLifecycleCallbacks]
class Portfolio
{
    use Entity\Id;
    use Entity\WebalizeName;
    use Entity\Title;
    use Entity\Content;
    use Entity\IsActive;
    use Entity\ImagePreview;
    use Entity\Sequence;
    use Entity\Link;

    #[ORM\ManyToOne(targetEntity: PortfolioType::class, inversedBy: 'portfolios')]
    #[ORM\JoinColumn(onDelete: 'cascade')]
    private PortfolioType $portfolioType;

    public function update(string $name, string $title, string $content, bool $isActive, string $link, PortfolioType $portfolioType, ?string $imagePreview): void
    {
        $this->name     = $name;
        $this->title    = $title;
        $this->content  = $content;
        $this->isActive = $isActive;
        $this->link     = $link;

        $this->portfolioType = $portfolioType;

        if ($imagePreview === null || $imagePreview === '') {
            return;
        }

        $this->imagePreview = $imagePreview;
    }

    public function updateWebalize(string $webalize): void
    {
        $this->webalize = $webalize;
    }

    public function getPortfolioType(): PortfolioType
    {
        return $this->portfolioType;
    }
}
