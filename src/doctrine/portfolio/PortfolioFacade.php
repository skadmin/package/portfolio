<?php

declare(strict_types=1);

namespace Skadmin\Portfolio\Doctrine\Portfolio;

use Doctrine\ORM\QueryBuilder;
use Nettrine\ORM\EntityManagerDecorator;
use Skadmin\Portfolio\Doctrine\PortfolioType\PortfolioType;
use SkadminUtils\DoctrineTraits;
use SkadminUtils\DoctrineTraits\Facade;

use function intval;

final class PortfolioFacade extends Facade
{
    use DoctrineTraits\Facade\Sequence;
    use DoctrineTraits\Facade\Webalize;

    public function __construct(EntityManagerDecorator $em)
    {
        parent::__construct($em);
        $this->table = Portfolio::class;
    }

    public function create(string $name, string $title, string $content, bool $isActive, string $link, PortfolioType $portfolioType, ?string $imagePreview): Portfolio
    {
        return $this->update(null, $name, $title, $content, $isActive, $link, $portfolioType, $imagePreview);
    }

    public function update(?int $id, string $name, string $title, string $content, bool $isActive, string $link, PortfolioType $portfolioType, ?string $imagePreview, bool $changeWebalize = false): Portfolio
    {
        $portfolio = $this->get($id);
        $portfolio->update($name, $title, $content, $isActive, $link, $portfolioType, $imagePreview);

        if (! $portfolio->isLoaded()) {
            $portfolio->setWebalize($this->getValidWebalize($name));
            $portfolio->setSequence($this->getValidSequence());
        } elseif ($changeWebalize) {
            $portfolio->updateWebalize($this->getValidWebalize($name));
        }

        $this->em->persist($portfolio);
        $this->em->flush();

        return $portfolio;
    }

    public function get(?int $id = null): Portfolio
    {
        if ($id === null) {
            return new Portfolio();
        }

        $portfolio = parent::get($id);

        if ($portfolio === null) {
            return new Portfolio();
        }

        return $portfolio;
    }

    public function getModel(?string $table = null): QueryBuilder
    {
        return parent::getModel($table)
            ->orderBy('a.sequence');
    }

    /**
     * @return Portfolio[]
     */
    public function getAll(bool $onlyActive = false): array
    {
        $criteria = [];

        if ($onlyActive) {
            $criteria['isActive'] = true;
        }

        $orderBy = ['sequence' => 'ASC'];

        return $this->em
            ->getRepository($this->table)
            ->findBy($criteria, $orderBy);
    }

    /**
     * @return Portfolio[]
     */
    public function getByLimit(bool $onlyActive = false, ?int $limit = null, ?int $offset = null): array
    {
        $criteria = [];

        if ($onlyActive) {
            $criteria['isActive'] = true;
        }

        $orderBy = ['sequence' => 'ASC'];

        return $this->em
            ->getRepository($this->table)
            ->findBy($criteria, $orderBy, $limit, $offset);
    }

    public function findByWebalize(string $webalize): ?Portfolio
    {
        $criteria = ['webalize' => $webalize];

        return $this->em
            ->getRepository($this->table)
            ->findOneBy($criteria);
    }

    public function findNext(Portfolio $portfolio): ?Portfolio
    {
        $criteria = ['sequence' => intval($portfolio->getSequence()) + 1];

        $nextPortfolio = $this->em
            ->getRepository($this->table)
            ->findOneBy($criteria);

        if ($nextPortfolio !== null) {
            return $nextPortfolio;
        }

        $criteria = ['sequence' => 0];

        return $this->em
            ->getRepository($this->table)
            ->findOneBy($criteria);
    }
}
