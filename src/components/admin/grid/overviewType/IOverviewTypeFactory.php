<?php

declare(strict_types=1);

namespace Skadmin\Portfolio\Components\Admin;

interface IOverviewTypeFactory
{
    public function create(): OverviewType;
}
