<?php

declare(strict_types=1);

namespace Skadmin\Portfolio\Components\Admin;

use App\Model\System\APackageControl;
use App\Model\System\Flash;
use Nette\ComponentModel\IContainer;
use Nette\Security\User;
use Nette\Utils\Html;
use Skadmin\Portfolio\BaseControl;
use Skadmin\Portfolio\Doctrine\PortfolioType\PortfolioType;
use Skadmin\Portfolio\Doctrine\PortfolioType\PortfolioTypeFacade;
use Skadmin\Role\Doctrine\Role\Privilege;
use Skadmin\Translator\Translator;
use SkadminUtils\GridControls\UI\GridControl;
use SkadminUtils\GridControls\UI\GridDoctrine;
use WebLoader\Nette\JavaScriptLoader;
use WebLoader\Nette\LoaderFactory;

class OverviewType extends GridControl
{
    use APackageControl;

    private LoaderFactory $webLoader;
    private PortfolioTypeFacade $facade;

    public function __construct(PortfolioTypeFacade $facade, Translator $translator, User $user, LoaderFactory $webLoader)
    {
        parent::__construct($translator, $user);

        $this->facade    = $facade;
        $this->webLoader = $webLoader;
    }

    /**
     * @return static
     */
    public function setParent(?IContainer $parent, ?string $name = null): static
    {
        parent::setParent($parent, $name);

        if (! $this->isAllowed(BaseControl::RESOURCE, Privilege::READ)) {
            $this->getParent()->redirect(':Admin:Homepage:accessDenied');
        }

        return $this;
    }

    public function render(): void
    {
        $template = $this->getComponentTemplate();
        $template->setTranslator($this->translator);
        $template->setFile(__DIR__ . '/overviewType.latte');
        $template->render();
    }

    public function getTitle(): string
    {
        return 'portfolio.overview-type.title';
    }

    /**
     * @return JavaScriptLoader[]
     */
    public function getJs(): array
    {
        return [
            $this->webLoader->createJavaScriptLoader('jQueryUi'),
        ];
    }

    public function handleSort(?string $itemId, ?string $prevId, ?string $nextId): void
    {
        $this->facade->sort($itemId, $prevId, $nextId);

        $presenter = $this->getPresenterIfExists();
        if ($presenter !== null) {
            $presenter->flashMessage('grid.portfolio.overview-type.action.flash.sort.success', Flash::SUCCESS);
        }

        $this['grid']->reload();
    }

    protected function createComponentGrid(string $name): GridDoctrine
    {
        $grid = new GridDoctrine($this->getPresenter());

        // DEFAULT
        $grid->setPrimaryKey('id');
        $grid->setDataSource($this->facade->getModel()
            ->orderBy('a.sequence', 'ASC'));

        // DATA

        // COLUMNS
        $grid->addColumnText('name', 'grid.portfolio.overview-type.name')
            ->setRenderer(function (PortfolioType $portfolioType): Html {
                $webalize = Html::el('code', ['class' => 'text-muted small'])
                    ->setText($portfolioType->getWebalize());

                if ($this->isAllowed(BaseControl::RESOURCE, 'write')) {
                    $link = $this->getPresenter()->link('Component:default', [
                        'package' => new BaseControl(),
                        'render'  => 'edit-type',
                        'id'      => $portfolioType->getId(),
                    ]);

                    $name = Html::el('a', [
                        'href'  => $link,
                        'class' => 'font-weight-bold',
                    ]);
                } else {
                    $name = new Html();
                }

                $name->setText($portfolioType->getName());

                $result = new Html();
                $result->addHtml($name)
                    ->addHtml('<br>')
                    ->addHtml($webalize);

                return $result;
            });

        // FILTER
        $grid->addFilterText('name', 'grid.portfolio.overview-type.name', ['name', 'webalize']);

        // ACTION
        if ($this->isAllowed(BaseControl::RESOURCE, 'write')) {
            $grid->addAction('edit', 'grid.portfolio.overview-type.action.edit', 'Component:default', ['id' => 'id'])->addParameters([
                'package' => new BaseControl(),
                'render'  => 'edit-type',
            ])->setIcon('pencil-alt')
                ->setClass('btn btn-xs btn-default btn-primary');
        }

        // TOOLBAR
        $grid->addToolbarButton('Component:default#1', 'grid.portfolio.overview.action.portfolio', [
            'package' => new BaseControl(),
            'render'  => 'overview',
        ])->setIcon('chevron-left')
            ->setClass('btn btn-xs btn-outline-primary');

        if ($this->isAllowed(BaseControl::RESOURCE, 'write')) {
            $grid->addToolbarButton('Component:default', 'grid.portfolio.overview-type.action.new', [
                'package' => new BaseControl(),
                'render'  => 'edit-type',
            ])->setIcon('plus')
                ->setClass('btn btn-xs btn-default btn-primary');
        }

        // SORTING
        $grid->setSortable();
        $grid->setSortableHandler($this->link('sort!'));

        return $grid;
    }
}
