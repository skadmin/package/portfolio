<?php

declare(strict_types=1);

namespace Skadmin\Portfolio\Components\Admin;

use App\Model\Grid\Traits\IsActive;
use App\Model\System\APackageControl;
use App\Model\System\Constant;
use App\Model\System\Flash;
use Nette\ComponentModel\IContainer;
use Nette\Security\User;
use Nette\Utils\Html;
use Skadmin\Portfolio\BaseControl;
use Skadmin\Portfolio\Doctrine\Portfolio\Portfolio;
use Skadmin\Portfolio\Doctrine\Portfolio\PortfolioFacade;
use Skadmin\Portfolio\Doctrine\PortfolioType\PortfolioTypeFacade;
use Skadmin\Role\Doctrine\Role\Privilege;
use Skadmin\Translator\Translator;
use SkadminUtils\GridControls\UI\GridControl;
use SkadminUtils\GridControls\UI\GridDoctrine;
use SkadminUtils\ImageStorage\ImageStorage;
use WebLoader\Nette\CssLoader;
use WebLoader\Nette\JavaScriptLoader;
use WebLoader\Nette\LoaderFactory;

use function sprintf;

class Overview extends GridControl
{
    use APackageControl;
    use IsActive;

    private LoaderFactory $webLoader;
    private PortfolioFacade $facade;
    private PortfolioTypeFacade $facadePortfolioType;
    private ImageStorage $imageStorage;

    public function __construct(PortfolioFacade $facade, PortfolioTypeFacade $facadePortfolioType, Translator $translator, User $user, LoaderFactory $webLoader, ImageStorage $imageStorage)
    {
        parent::__construct($translator, $user);

        $this->facade              = $facade;
        $this->facadePortfolioType = $facadePortfolioType;

        $this->webLoader    = $webLoader;
        $this->imageStorage = $imageStorage;
    }

    /**
     * @return static
     */
    public function setParent(?IContainer $parent, ?string $name = null): static
    {
        parent::setParent($parent, $name);

        if (! $this->isAllowed(BaseControl::RESOURCE, Privilege::READ)) {
            $this->getParent()->redirect(':Admin:Homepage:accessDenied');
        }

        return $this;
    }

    public function render(): void
    {
        $template = $this->getComponentTemplate();
        $template->setTranslator($this->translator);
        $template->setFile(__DIR__ . '/overview.latte');
        $template->render();
    }

    public function getTitle(): string
    {
        return 'portfolio.overview.title';
    }

    /**
     * @return CssLoader[]
     */
    public function getCss(): array
    {
        return [
            $this->webLoader->createCssLoader('customFileInput'),
        ];
    }

    /**
     * @return JavaScriptLoader[]
     */
    public function getJs(): array
    {
        return [
            $this->webLoader->createJavaScriptLoader('customFileInput'),
            $this->webLoader->createJavaScriptLoader('jQueryUi'),
        ];
    }

    public function handleSort(?string $itemId, ?string $prevId, ?string $nextId): void
    {
        $this->facade->sort($itemId, $prevId, $nextId);

        $presenter = $this->getPresenterIfExists();
        if ($presenter !== null) {
            $presenter->flashMessage('grid.portfolio.overview.action.flash.sort.success', Flash::SUCCESS);
        }

        $this['grid']->reload();
    }

    protected function createComponentGrid(string $name): GridDoctrine
    {
        $grid = new GridDoctrine($this->getPresenter());

        // DATA
        $dataPortfolioType = $this->facadePortfolioType->getPairs('id', 'name');

        // DEFAULT
        $grid->setPrimaryKey('id');
        $grid->setDataSource($this->facade->getModel());

        // COLUMNS
        $grid->addColumnText('imagePreview', '')
            ->setRenderer(function (Portfolio $portfolio): ?Html {
                if ($portfolio->getImagePreview() !== null) {
                    $imageSrc = $this->imageStorage->fromIdentifier([$portfolio->getImagePreview(), '120x60', 'shrink_only']);

                    return Html::el('img', [
                        'src'   => sprintf('/%s', $imageSrc->createLink()),
                        'style' => 'max-width: none;',
                    ]);
                }

                return null;
            })->setAlign('center');
        $grid->addColumnText('name', 'grid.portfolio.overview.name')
            ->setRenderer(function (Portfolio $portfolio): Html {
                $webalize = Html::el('code', ['class' => 'text-muted small'])
                    ->setText($portfolio->getWebalize());

                if ($this->isAllowed(BaseControl::RESOURCE, 'write')) {
                    $link = $this->getPresenter()->link('Component:default', [
                        'package' => new BaseControl(),
                        'render'  => 'edit',
                        'id'      => $portfolio->getId(),
                    ]);

                    $name = Html::el('a', [
                        'href'  => $link,
                        'class' => 'font-weight-bold',
                    ]);
                } else {
                    $name = new Html();
                }

                $name->setText($portfolio->getName());

                $result = new Html();
                $result->addHtml($name)
                    ->addHtml('<br>')
                    ->addHtml($webalize);

                if ($portfolio->getLink() !== '') {
                    $link = Html::el('a', [
                        'class' => 'text-muted small',
                        'target' => '_blank',
                        'href'  => $portfolio->getLink(),
                    ])->setText($portfolio->getLink());

                    $result->addHtml('<br>')
                        ->addHtml($link);
                }

                return $result;
            });
        $grid->addColumnText('title', 'grid.portfolio.overview.title');
        $grid->addColumnText('portfolioType', 'grid.portfolio.overview.portfolio-type', 'portfolioType.name');
        $this->addColumnIsActive($grid, 'portfolio.overview');

        // STYLE
        $grid->getColumn('imagePreview')
            ->getElementPrototype('th')
            ->setAttribute('style', 'width: 1px');

        // FILTER
        $grid->addFilterText('name', 'grid.portfolio.overview.name', ['name', 'webalize']);
        $grid->addFilterText('title', 'grid.portfolio.overview.title');
        $grid->addFilterSelect('portfolioType', 'grid.portfolio.overview.portfolio-type', $dataPortfolioType, 'portfolioType')
            ->setPrompt(Constant::PROMTP);
        $this->addFilterIsActive($grid, 'portfolio.overview');

        // ACTION
        if ($this->isAllowed(BaseControl::RESOURCE, 'write')) {
            $grid->addAction('edit', 'grid.portfolio.overview.action.edit', 'Component:default', ['id' => 'id'])
                ->addParameters([
                    'package' => new BaseControl(),
                    'render'  => 'edit',
                ])->setIcon('pencil-alt')
                ->setClass('btn btn-xs btn-primary');

            $grid->addAction('duplicate', 'grid.portfolio.overview.action.duplicate', 'Component:default', ['duplicateId' => 'id'])
                ->setTitle('grid.portfolio.overview.action.duplicate.duplicate')
                ->setDataAttribute('toggle', 'tooltip')
                ->setDataAttribute('placement', 'left')
                ->addParameters([
                    'package' => new BaseControl(),
                    'render'  => 'edit',
                ])->setIcon('clone')
                ->setClass('btn btn-xs btn-outline-primary');
        }

        // TOOLBAR
        $grid->addToolbarButton('Component:default#1', 'grid.portfolio.overview.action.portfolio-type', [
            'package' => new BaseControl(),
            'render'  => 'overview-type',
        ])->setIcon('list')
            ->setClass('btn btn-xs btn-outline-primary');

        if ($this->isAllowed(BaseControl::RESOURCE, 'write')) {
            $grid->addToolbarButton('Component:default', 'grid.portfolio.overview.action.new', [
                'package' => new BaseControl(),
                'render'  => 'edit',
            ])->setIcon('plus')
                ->setClass('btn btn-xs btn-primary');
        }

        $grid->addToolbarSeo(BaseControl::RESOURCE, $this->getTitle());

        // SORTING
        $grid->setSortable();
        $grid->setSortableHandler($this->link('sort!'));

        return $grid;
    }
}
