<?php

declare(strict_types=1);

namespace Skadmin\Portfolio\Components\Admin;

interface IOverviewFactory
{
    public function create(): Overview;
}
