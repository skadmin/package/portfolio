<?php

declare(strict_types=1);

namespace Skadmin\Portfolio\Components\Admin;

interface IEditTypeFactory
{
    public function create(?int $id = null): EditType;
}
