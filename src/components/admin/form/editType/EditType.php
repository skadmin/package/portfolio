<?php

declare(strict_types=1);

namespace Skadmin\Portfolio\Components\Admin;

use App\Model\System\APackageControl;
use App\Model\System\Flash;
use Nette\ComponentModel\IContainer;
use Nette\Security\User as LoggedUser;
use Nette\Utils\ArrayHash;
use Nette\Utils\Html;
use Skadmin\Portfolio\BaseControl;
use Skadmin\Portfolio\Doctrine\PortfolioType\PortfolioType;
use Skadmin\Portfolio\Doctrine\PortfolioType\PortfolioTypeFacade;
use Skadmin\Role\Doctrine\Role\Privilege;
use Skadmin\Translator\SimpleTranslation;
use Skadmin\Translator\Translator;
use SkadminUtils\FormControls\UI\Form;
use SkadminUtils\FormControls\UI\FormWithUserControl;
use SkadminUtils\ImageStorage\ImageStorage;
use WebLoader\Nette\CssLoader;
use WebLoader\Nette\JavaScriptLoader;
use WebLoader\Nette\LoaderFactory;

class EditType extends FormWithUserControl
{
    use APackageControl;

    private LoaderFactory $webLoader;
    private PortfolioTypeFacade $facade;
    private PortfolioType $portfolioType;
    private ImageStorage $imageStorage;

    public function __construct(?int $id, PortfolioTypeFacade $facade, Translator $translator, LoaderFactory $webLoader, LoggedUser $user, ImageStorage $imageStorage)
    {
        parent::__construct($translator, $user);
        $this->facade       = $facade;
        $this->webLoader    = $webLoader;
        $this->imageStorage = $imageStorage;

        $this->portfolioType = $this->facade->get($id);
    }

    /**
     * @return static
     */
    public function setParent(?IContainer $parent, ?string $name = null): static
    {
        parent::setParent($parent, $name);

        if (! $this->isAllowed(BaseControl::RESOURCE, Privilege::WRITE)) {
            $this->getParent()->redirect(':Admin:Homepage:accessDenied');
        }

        return $this;
    }

    public function getTitle(): SimpleTranslation|string
    {
        if ($this->portfolioType->isLoaded()) {
            return new SimpleTranslation('portfolio-type.edit.title - %s', $this->portfolioType->getName());
        }

        return 'portfolio-type.edit.title';
    }

    /**
     * @return CssLoader[]
     */
    public function getCss(): array
    {
        return [
            $this->webLoader->createCssLoader('customFileInput'),
        ];
    }

    /**
     * @return JavaScriptLoader[]
     */
    public function getJs(): array
    {
        return [
            $this->webLoader->createJavaScriptLoader('adminTinyMce'),
            $this->webLoader->createJavaScriptLoader('customFileInput'),
        ];
    }

    public function processOnSuccess(Form $form, ArrayHash $values): void
    {
        if ($this->portfolioType->isLoaded()) {
            $portfolioType = $this->facade->update(
                $this->portfolioType->getId(),
                $values->name,
                $values->content,
                $values->change_webalize
            );
            $this->onFlashmessage('form.portfolio-type.edit.flash.success.update', Flash::SUCCESS);
        } else {
            $portfolioType = $this->facade->create(
                $values->name,
                $values->content
            );
            $this->onFlashmessage('form.portfolio-type.edit.flash.success.create', Flash::SUCCESS);
        }

        if ($form->isSubmitted()->name === 'send_back') {
            $this->processOnBack();
        }

        $this->getPresenter()->redirect('Component:default', [
            'package' => new BaseControl(),
            'render'  => 'edit-type',
            'id'      => $portfolioType->getId(),
        ]);
    }

    public function processOnBack(): void
    {
        $this->getPresenter()->redirect('Component:default', [
            'package' => new BaseControl(),
            'render'  => 'overview-type',
        ]);
    }

    public function render(): void
    {
        $template               = $this->getComponentTemplate();
        $template->imageStorage = $this->imageStorage;
        $template->setTranslator($this->translator);
        $template->setFile(__DIR__ . '/editType.latte');

        $template->portfolioType = $this->portfolioType;
        $template->render();
    }

    protected function createComponentForm(): Form
    {
        $form = new Form();
        $form->setTranslator($this->translator);

        // INPUT
        $form->addText('name', 'form.portfolio-type.edit.name')
            ->setRequired('form.portfolio-type.edit.name.req');
        $form->addTextArea('content', 'form.portfolio-type.edit.content');

        // CHANGE WEBALIZE
        if ($this->portfolioType->isLoaded()) {
            $form->addCheckbox('change_webalize', Html::el('sup', [
                'title'          => $this->translator->translate('form.portfolio-type.edit.change-webalize'),
                'class'          => 'far fa-fw fa-question-circle',
                'data-toggle'    => 'tooltip',
                'data-placement' => 'left',
            ]))->setTranslator(null);
        }

        // BUTTON
        $form->addSubmit('send', 'form.portfolio-type.edit.send');
        $form->addSubmit('send_back', 'form.portfolio-type.edit.send-back');
        $form->addSubmit('back', 'form.portfolio-type.edit.back')
            ->setValidationScope([])
            ->onClick[] = [$this, 'processOnBack'];

        // DEFAULT
        $form->setDefaults($this->getDefaults());

        // CALLBACK
        $form->onSuccess[] = [$this, 'processOnSuccess'];

        return $form;
    }

    /**
     * @return mixed[]
     */
    private function getDefaults(): array
    {
        if (! $this->portfolioType->isLoaded()) {
            return [];
        }

        return [
            'name'    => $this->portfolioType->getName(),
            'content' => $this->portfolioType->getContent(),
        ];
    }
}
