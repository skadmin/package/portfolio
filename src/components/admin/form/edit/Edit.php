<?php

declare(strict_types=1);

namespace Skadmin\Portfolio\Components\Admin;

use App\Model\System\APackageControl;
use App\Model\System\Constant;
use App\Model\System\Flash;
use Nette\ComponentModel\IContainer;
use Nette\Http\Request;
use Nette\Security\User;
use Nette\Utils\ArrayHash;
use Nette\Utils\Html;
use Skadmin\Portfolio\BaseControl;
use Skadmin\Portfolio\Doctrine\Portfolio\Portfolio;
use Skadmin\Portfolio\Doctrine\Portfolio\PortfolioFacade;
use Skadmin\Portfolio\Doctrine\PortfolioType\PortfolioTypeFacade;
use Skadmin\Role\Doctrine\Role\Privilege;
use Skadmin\Translator\SimpleTranslation;
use Skadmin\Translator\Translator;
use SkadminUtils\FormControls\UI\Form;
use SkadminUtils\FormControls\UI\FormWithUserControl;
use SkadminUtils\FormControls\Utils\UtilsFormControl;
use SkadminUtils\ImageStorage\ImageStorage;
use WebLoader\Nette\CssLoader;
use WebLoader\Nette\JavaScriptLoader;
use WebLoader\Nette\LoaderFactory;

use function intval;
use function sprintf;

class Edit extends FormWithUserControl
{
    use APackageControl;

    private Request $request;
    private LoaderFactory $webLoader;
    private PortfolioFacade $facade;
    private PortfolioTypeFacade $facadePortfolioType;
    private Portfolio $portfolio;
    private ImageStorage $imageStorage;

    public function __construct(?int $id, Request $request, PortfolioFacade $facade, PortfolioTypeFacade $facadePortfolioType, Translator $translator, LoaderFactory $webLoader, User $user, ImageStorage $imageStorage)
    {
        parent::__construct($translator, $user);

        $this->request             = $request;
        $this->facade              = $facade;
        $this->facadePortfolioType = $facadePortfolioType;

        $this->webLoader    = $webLoader;
        $this->imageStorage = $imageStorage;

        $this->portfolio = $this->facade->get($id);
    }

    /**
     * @return static
     */
    public function setParent(?IContainer $parent, ?string $name = null): static
    {
        parent::setParent($parent, $name);

        if (! $this->isAllowed(BaseControl::RESOURCE, Privilege::WRITE)) {
            $this->getParent()->redirect(':Admin:Homepage:accessDenied');
        }

        return $this;
    }

    public function getTitle(): SimpleTranslation|string
    {
        if ($this->portfolio->isLoaded()) {
            return new SimpleTranslation('portfolio.edit.title - %s', $this->portfolio->getName());
        }

        return 'portfolio.edit.title';
    }

    /**
     * @return CssLoader[]
     */
    public function getCss(): array
    {
        return [
            $this->webLoader->createCssLoader('customFileInput'),
            $this->webLoader->createCssLoader('fancyBox'), // responsive file manager
        ];
    }

    /**
     * @return JavaScriptLoader[]
     */
    public function getJs(): array
    {
        return [
            $this->webLoader->createJavaScriptLoader('adminTinyMce'),
            $this->webLoader->createJavaScriptLoader('customFileInput'),
            $this->webLoader->createJavaScriptLoader('fancyBox'), // responsive file manager
        ];
    }

    public function processOnSuccess(Form $form, ArrayHash $values): void
    {
        // IDENTIFIER
        $identifier = UtilsFormControl::getImagePreview($values->image_preview, BaseControl::DIR_IMAGE);

        if ($this->portfolio->isLoaded()) {
            if ($identifier !== null && $this->portfolio->getImagePreview() !== null) {
                $this->imageStorage->delete($this->portfolio->getImagePreview());
            }

            $portfolio = $this->facade->update(
                $this->portfolio->getId(),
                $values->name,
                $values->title,
                $values->content,
                $values->is_active,
                $values->link,
                $this->facadePortfolioType->get($values->portfolio_type_id),
                $identifier,
                $values->change_webalize
            );
            $this->onFlashmessage('form.portfolio.edit.flash.success.update', Flash::SUCCESS);
        } else {
            $portfolio = $this->facade->create(
                $values->name,
                $values->title,
                $values->content,
                $values->is_active,
                $values->link,
                $this->facadePortfolioType->get($values->portfolio_type_id),
                $identifier
            );
            $this->onFlashmessage('form.portfolio.edit.flash.success.create', Flash::SUCCESS);
        }

        if ($form->isSubmitted()->name === 'send_back') {
            $this->processOnBack();
        }

        $this->getPresenter()->redirect('Component:default', [
            'package' => new BaseControl(),
            'render'  => 'edit',
            'id'      => $portfolio->getId(),
        ]);
    }

    public function processOnBack(): void
    {
        $this->getPresenter()->redirect('Component:default', [
            'package' => new BaseControl(),
            'render'  => 'overview',
        ]);
    }

    public function render(): void
    {
        $template               = $this->getComponentTemplate();
        $template->imageStorage = $this->imageStorage;
        $template->setTranslator($this->translator);
        $template->setFile(__DIR__ . '/edit.latte');

        $template->portfolio = $this->portfolio;
        $template->render();
    }

    protected function createComponentForm(): Form
    {
        $form = new Form();
        $form->setTranslator($this->translator);

        // DATA
        $dataPortfolioType = $this->facadePortfolioType->getPairs('id', 'name');

        // INPUT
        $form->addText('name', 'form.portfolio.edit.name')
            ->setRequired('form.portfolio.edit.name.req');
        $form->addText('title', 'form.portfolio.edit.title');
        $form->addTextArea('content', 'form.portfolio.edit.content');
        $form->addCheckbox('is_active', 'form.portfolio.edit.is-active')
            ->setDefaultValue(true);

        $form->addUrl('link', 'form.portfolio.edit.link', []);

        $form->addImageWithRFM('image_preview', 'form.portfolio.edit.image-preview');

        // CHANGE WEBALIZE
        if ($this->portfolio->isLoaded()) {
            $form->addCheckbox('change_webalize', Html::el('sup', [
                'title'          => $this->translator->translate('form.portfolio.edit.change-webalize'),
                'class'          => 'far fa-fw fa-question-circle',
                'data-toggle'    => 'tooltip',
                'data-placement' => 'left',
            ]))->setTranslator(null);
        }

        // TYPE
        $form->addSelect('portfolio_type_id', 'form.portfolio.edit.portfolio-type-id', $dataPortfolioType)
            ->setRequired('form.portfolio.edit.portfolio-type-id.req')
            ->setPrompt(Constant::PROMTP)
            ->setTranslator(null);

        // BUTTON
        $form->addSubmit('send', 'form.portfolio.edit.send');
        $form->addSubmit('send_back', 'form.portfolio.edit.send-back');
        $form->addSubmit('back', 'form.portfolio.edit.back')
            ->setValidationScope([])
            ->onClick[] = [$this, 'processOnBack'];

        // DEFAULT
        $form->setDefaults($this->getDefaults());

        // CALLBACK
        $form->onSuccess[] = [$this, 'processOnSuccess'];

        return $form;
    }

    /**
     * @return mixed[]
     */
    private function getDefaults(): array
    {
        $duplicateId = $this->request->getUrl()->getQueryParameter('duplicateId');
        $data        = [];

        if ($this->portfolio->isLoaded()) {
            $data = [
                'name'              => $this->portfolio->getName(),
                'title'             => $this->portfolio->getTitle(),
                'content'           => $this->portfolio->getContent(),
                'is_active'         => $this->portfolio->isActive(),
                'link'              => $this->portfolio->getLink(),
                'portfolio_type_id' => $this->portfolio->getPortfolioType()->getId(),
            ];
        } elseif ($duplicateId !== null) {
            $duplicatePortfolio = $this->facade->get(intval($duplicateId));

            if ($duplicatePortfolio->isLoaded()) {
                $data = [
                    'name'              => sprintf('Duplicate - %s', $duplicatePortfolio->getName()),
                    'title'             => $duplicatePortfolio->getTitle(),
                    'content'           => $duplicatePortfolio->getContent(),
                    'is_active'         => false,
                    'link'              => $duplicatePortfolio->getLink(),
                    'portfolio_type_id' => $duplicatePortfolio->getPortfolioType()->getId(),
                ];
            }
        }

        return $data;
    }
}
